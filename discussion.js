// CRUD Operations
/* Create - make table
	Read - find
	update -
	delete -

*/

// Inserting document(Create)
// Synatx: db.collectionName.insertOne({object});
//  js: object.object.method({object});


db.users.insertOne({
    "firstName":"Jane",
    "lastName": "Doo",
    "age": 21,
    "contact": {
        "phone": "09876543211",
        "email": "janedoe@gmail.com"
        },
     "courses" :["CSS","Javascript","Python"],
     "department" : "none" 
    });



// insert many
// syntax: db.colectionName([{objectA},{objecB}])
db.users.insertMany([{
    "firstName":"Stephen",
    "lastName": "Hawking",
    "age": 76,
    "contact": {
        "phone": "0921212121",
        "email": "stephenhawking@gmail.com"
        },
     "courses" :["React","PHO","Python"],
     "department" : "none"  
    
    
    },
    {
    "firstName":"Noven",
    "lastName": "gorospe",
    "age": 24,
    "contact": {
        "phone": "123113311",
        "email": "noven@gmail.com"
        },
     "courses" :["React","laravel","SASS"],
     "department" : "none"  
    
    
    }
    ]);


// Finding documents (Read) operation
/* find syntax:
	db.collectionName.find();
	or
	db.collectionName.find(field: value);
*/
db.users.find();

db.users.find({lastName: "Doo"});

db.users.find({lastName: "Doo","age": 21}).pretty();

// db.users.find({"_id": objectID})

// Updating Documents (Update) operation
// syntax : db.collectionName.updateOne({criteria}, {$set: {field: values}});


db.users.insertOne({
    "firstName":"Test",
    "lastName": "Test",
    "age": 0,
    "contact": {
        "phone": "00000000000",
        "email": "test@gmail.com"
        },
     "courses" :[],
     "department" : "none" 
    });



// Upadate single document
db.users.updateOne(
    {"firstName": "Test"},
    {
        
            $set: {
            "firstName":"Bill",
            "lastName": "Gates",
            "age": 65,
            "contact": {
                "phone": "0917012065",
                "email": "bill@gmail.com"
                },
             "courses" :["PHP","Laravel","HTML"],
             "department" : "none"   
       }
    }
  );


// update multiple documents
db.users.updateMany(
    {"department": "none"},
    {
        $set:{"department" : "HR"}
        }

    );


// update multiple department in in same lastame




// Delete documents (Delete) operation
// syntax: db.collectionName.deleteOne({criteria}); deleting single documennt
// 


db.users.deleteOne({
    "firstName": "test",
    })



// Update multiple documents where lastName is "Doe"
db.users.UpdateMany(
	{ "lastName": "Doe" },
	{
		$set: {
			"department": "Operations"
		}
	}
);



// Delete multiple documents
db.users.deleteMany({
    "department": "Operations",
    });

// Query and embedded document
db.users.find({
	"contact":{
		 "phone": "0917012065",
         "email": "bill@gmail.com"
	}
});


//  Querying an array without a specific elements

db.users.find({
	"courses": { $all : ["React", "Python"]}
});
